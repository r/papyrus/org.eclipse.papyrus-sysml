/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *  
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.service.types.tests.util;

import java.lang.reflect.Field;
import java.util.Map;

import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.types.ElementTypeSetConfiguration;
import org.eclipse.papyrus.infra.types.core.registries.ElementTypeSetConfigurationRegistry;
import org.eclipse.papyrus.sysml14.service.types.util.SysMLServiceTypeUtil;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 * The class should test that all constant in SysMLServiceTypeUtil should be referring 
 * to an existing element type
 * @author Benoit Maggi
 *
 */
@SuppressWarnings("nls")
public class SysMLServiceTypeUtilTest {

	/**
	 * Check that the required element type set are correctly loaded
	 */
	@BeforeClass
	public static void loadSysml14Defintion() {
		ElementTypeSetConfigurationRegistry instance = ElementTypeSetConfigurationRegistry.getInstance();
		Map<String, Map<String, ElementTypeSetConfiguration>> elementTypeSetConfigurations = instance.getElementTypeSetConfigurations();
		Map<String, ElementTypeSetConfiguration> map = elementTypeSetConfigurations.get(SysMLServiceTypeUtil.SYSML14_CONTEXT_ID);
		ElementTypeSetConfiguration elementTypeSetConfiguration = map.get("org.eclipse.papyrus.sysml14.service.types.elementTypeSet");
		Assert.assertNotNull("The SysML 1.4 element type set definition is missing", elementTypeSetConfiguration);
		ElementTypeSetConfiguration elementTypeSetConfigurationExtension = map.get("org.eclipse.papyrus.sysml14.service.types.elementTypeSet.extension");
		Assert.assertNotNull("The SysML 1.4 element type set extension definition is missing", elementTypeSetConfigurationExtension);
		
	}
	
	
    /**
     * Check that each field is referencing an existing element type
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws ServiceException 
     */
    @Test
	public void testElementTypeIdsDefinedAsConstant() throws IllegalArgumentException, IllegalAccessException, ServiceException {	
		Field[] declaredFields = SysMLServiceTypeUtil.class.getDeclaredFields();
		for (int i = 0; i < declaredFields.length; i++) {
			Field field = declaredFields[i];
			if (field.isAccessible()){
				String name = field.getName();
				Object value = field.get(SysMLServiceTypeUtil.class);
				Assert.assertTrue(name+" value should define as a String", value instanceof String);
				boolean knownElementType = SysMLServiceTypeUtil.isKnown((String) value);
				Assert.assertTrue(name+" value should define an existing elementtype id", knownElementType);				
			}
		}
	}
}
