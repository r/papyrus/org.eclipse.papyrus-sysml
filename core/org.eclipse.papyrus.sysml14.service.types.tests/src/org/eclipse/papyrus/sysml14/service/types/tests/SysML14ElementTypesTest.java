/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *  
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.service.types.tests;

import java.util.Map;

import org.eclipse.papyrus.infra.types.ElementTypeSetConfiguration;
import org.eclipse.papyrus.infra.types.core.registries.ElementTypeSetConfigurationRegistry;
import org.eclipse.papyrus.sysml14.service.types.util.SysMLServiceTypeUtil;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * This class is to check any change in the SysML 1.4 element type id
 * (since the ids can be used a specialization they are part of public API)
 *
 */
@SuppressWarnings("nls")
public class SysML14ElementTypesTest {

	@BeforeClass
	public static void loadSysml14Defintion() {
		ElementTypeSetConfigurationRegistry instance = ElementTypeSetConfigurationRegistry.getInstance();
		Map<String, Map<String, ElementTypeSetConfiguration>> elementTypeSetConfigurations = instance.getElementTypeSetConfigurations();
		Map<String, ElementTypeSetConfiguration> map = elementTypeSetConfigurations.get(SysMLServiceTypeUtil.SYSML14_CONTEXT_ID);
		ElementTypeSetConfiguration elementTypeSetConfiguration = map.get("org.eclipse.papyrus.sysml14.service.types.elementTypeSet");
		Assert.assertNotNull("The SysML 1.4 element type set definition is missing", elementTypeSetConfiguration);
	}

    @Test
	public void testRegistryContentForAcceptChangeStructuralFeatureEventAction() {
		Assert.assertTrue("AcceptChangeStructuralFeatureEventAction element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.AcceptChangeStructuralFeatureEventAction"));
	}

    @Test
	public void testRegistryContentForAdjunctProperty() {
		Assert.assertTrue("AdjunctProperty element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.AdjunctProperty"));
	}

    @Test
	public void testRegistryContentForAllocateActivityPartition() {
		Assert.assertTrue("AllocateActivityPartition element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.AllocateActivityPartition"));
	}

    @Test
	public void testRegistryContentForAllocateAbstraction() {
		Assert.assertTrue("Allocate Abstraction element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Allocate_Abstraction"));
	}

    @Test
	public void testRegistryContentForAllocateDirectedRelationship() {
		Assert.assertTrue("Allocate DirectedRelationship element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Allocate_DirectedRelationship"));
	}

    @Test
	public void testRegistryContentForBindingConnector() {
		Assert.assertTrue("BindingConnector element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.BindingConnector"));
	}

    @Test
	public void testRegistryContentForBlock() {
		Assert.assertTrue("Block element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Block"));
	}

    @Test
	public void testRegistryContentForBoundReference() {
		Assert.assertTrue("BoundReference element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.BoundReference"));
	}

    @Test
	public void testRegistryContentForChangeStructuralFeatureEvent() {
		Assert.assertTrue("ChangeStructuralFeatureEvent element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ChangeStructuralFeatureEvent"));
	}

    @Test
	public void testRegistryContentForClassifierBehaviorProperty() {
		Assert.assertTrue("ClassifierBehaviorProperty element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ClassifierBehaviorProperty"));
	}

    @Test
	public void testRegistryContentForConform() {
		Assert.assertTrue("Conform element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Conform"));
	}

    @Test
	public void testRegistryContentForConnectorProperty() {
		Assert.assertTrue("ConnectorProperty element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ConnectorProperty"));
	}

    @Test
	public void testRegistryContentForConstraintBlock() {
		Assert.assertTrue("ConstraintBlock element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ConstraintBlock"));
	}

    @Test
	public void testRegistryContentForContinuousActivityEdge() {
		Assert.assertTrue("Continuous ActivityEdge element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Continuous_ActivityEdge"));
	}

    @Test
	public void testRegistryContentForContinuousObjectNode() {
		Assert.assertTrue("Continuous ObjectNode element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Continuous_ObjectNode"));
	}

    @Test
	public void testRegistryContentForContinuousParameter() {
		Assert.assertTrue("Continuous Parameter element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Continuous_Parameter"));
	}

    @Test
	public void testRegistryContentForControlOperatorBehavior() {
		Assert.assertTrue("ControlOperator Behavior element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ControlOperator_Behavior"));
	}

    @Test
	public void testRegistryContentForControlOperatorOperation() {
		Assert.assertTrue("ControlOperator Operation element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ControlOperator_Operation"));
	}

    @Test
	public void testRegistryContentForCopyAbstraction() {
		Assert.assertTrue("Copy Abstraction element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Copy_Abstraction"));
	}

    @Test
	public void testRegistryContentForCopyDirectedRelationship() {
		Assert.assertTrue("Copy DirectedRelationship element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Copy_DirectedRelationship"));
	}

    @Test
	public void testRegistryContentForDeriveReqtAbstraction() {
		Assert.assertTrue("DeriveReqt Abstraction element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.DeriveReqt_Abstraction"));
	}

    @Test
	public void testRegistryContentForDeriveReqtDirectedRelationship() {
		Assert.assertTrue("DeriveReqt DirectedRelationship element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.DeriveReqt_DirectedRelationship"));
	}

    @Test
	public void testRegistryContentForDirectedFeature() {
		Assert.assertTrue("DirectedFeature element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.DirectedFeature"));
	}

    @Test
	public void testRegistryContentForDirectedRelationshipPropertyPath() {
		Assert.assertTrue("DirectedRelationshipPropertyPath element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.DirectedRelationshipPropertyPath"));
	}

    @Test
	public void testRegistryContentForDiscreteActivityEdge() {
		Assert.assertTrue("Discrete ActivityEdge element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Discrete_ActivityEdge"));
	}

    @Test
	public void testRegistryContentForDiscreteObjectNode() {
		Assert.assertTrue("Discrete ObjectNode element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Discrete_ObjectNode"));
	}

    @Test
	public void testRegistryContentForDiscreteParameter() {
		Assert.assertTrue("Discrete Parameter element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Discrete_Parameter"));
	}

    @Test
	public void testRegistryContentForDistributedProperty() {
		Assert.assertTrue("DistributedProperty element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.DistributedProperty"));
	}

    @Test
	public void testRegistryContentForElementGroup() {
		Assert.assertTrue("ElementGroup element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ElementGroup"));
	}

    @Test
	public void testRegistryContentForElementPropertyPath() {
		Assert.assertTrue("ElementPropertyPath element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ElementPropertyPath"));
	}

    @Test
	public void testRegistryContentForEndPathMultiplicity() {
		Assert.assertTrue("EndPathMultiplicity element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.EndPathMultiplicity"));
	}

    @Test
	public void testRegistryContentForExpose() {
		Assert.assertTrue("Expose element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Expose"));
	}

    @Test
	public void testRegistryContentForFlowPort() {
		Assert.assertTrue("FlowPort element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.FlowPort"));
	}

    @Test
	public void testRegistryContentForFlowProperty() {
		Assert.assertTrue("FlowProperty element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.FlowProperty"));
	}

    @Test
	public void testRegistryContentForFlowSpecification() {
		Assert.assertTrue("FlowSpecification element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.FlowSpecification"));
	}

    @Test
	public void testRegistryContentForFullPort() {
		Assert.assertTrue("FullPort element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.FullPort"));
	}

    @Test
	public void testRegistryContentForInterfaceBlock() {
		Assert.assertTrue("InterfaceBlock element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.InterfaceBlock"));
	}

    @Test
	public void testRegistryContentForInvocationOnNestedPortActionElement() {
		Assert.assertTrue("InvocationOnNestedPortAction Element element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.InvocationOnNestedPortAction_Element"));
	}

    @Test
	public void testRegistryContentForInvocationOnNestedPortActionInvocationAction() {
		Assert.assertTrue("InvocationOnNestedPortAction InvocationAction element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.InvocationOnNestedPortAction_InvocationAction"));
	}

    @Test
	public void testRegistryContentForItemFlow() {
		Assert.assertTrue("ItemFlow element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ItemFlow"));
	}

    @Test
	public void testRegistryContentForNestedConnectorEndConnectorEnd() {
		Assert.assertTrue("NestedConnectorEnd ConnectorEnd element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.NestedConnectorEnd_ConnectorEnd"));
	}

    @Test
	public void testRegistryContentForNestedConnectorEndElement() {
		Assert.assertTrue("NestedConnectorEnd Element element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.NestedConnectorEnd_Element"));
	}

    @Test
	public void testRegistryContentForNoBuffer() {
		Assert.assertTrue("NoBuffer element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.NoBuffer"));
	}

    @Test
	public void testRegistryContentForOptional() {
		Assert.assertTrue("Optional element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Optional"));
	}

    @Test
	public void testRegistryContentForOverwrite() {
		Assert.assertTrue("Overwrite element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Overwrite"));
	}

    @Test
	public void testRegistryContentForParticipantProperty() {
		Assert.assertTrue("ParticipantProperty element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ParticipantProperty"));
	}

    @Test
	public void testRegistryContentForProbabilityActivityEdge() {
		Assert.assertTrue("Probability ActivityEdge element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Probability_ActivityEdge"));
	}

    @Test
	public void testRegistryContentForProbabilityParameterSet() {
		Assert.assertTrue("Probability ParameterSet element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Probability_ParameterSet"));
	}

    @Test
	public void testRegistryContentForProblem() {
		Assert.assertTrue("Problem element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Problem"));
	}

    @Test
	public void testRegistryContentForPropertySpecificType() {
		Assert.assertTrue("PropertySpecificType element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.PropertySpecificType"));
	}

    @Test
	public void testRegistryContentForProxyPort() {
		Assert.assertTrue("ProxyPort element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ProxyPort"));
	}

    @Test
	public void testRegistryContentForRateActivityEdge() {
		Assert.assertTrue("Rate ActivityEdge element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Rate_ActivityEdge"));
	}

    @Test
	public void testRegistryContentForRateObjectNode() {
		Assert.assertTrue("Rate ObjectNode element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Rate_ObjectNode"));
	}

    @Test
	public void testRegistryContentForRateParameter() {
		Assert.assertTrue("Rate Parameter element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Rate_Parameter"));
	}

    @Test
	public void testRegistryContentForRationale() {
		Assert.assertTrue("Rationale element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Rationale"));
	}

    @Test
	public void testRegistryContentForRefineAbstraction() {
		Assert.assertTrue("Refine Abstraction element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Refine_Abstraction"));
	}

    @Test
	public void testRegistryContentForRefineDirectedRelationship() {
		Assert.assertTrue("Refine DirectedRelationship element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Refine_DirectedRelationship"));
	}

    @Test
	public void testRegistryContentForRequirement() {
		Assert.assertTrue("Requirement element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Requirement"));
	}

    @Test
	public void testRegistryContentForSatisfyAbstraction() {
		Assert.assertTrue("Satisfy Abstraction element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Satisfy_Abstraction"));
	}

    @Test
	public void testRegistryContentForSatisfyDirectedRelationship() {
		Assert.assertTrue("Satisfy DirectedRelationship element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Satisfy_DirectedRelationship"));
	}

    @Test
	public void testRegistryContentForStakeholder() {
		Assert.assertTrue("Stakeholder element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Stakeholder"));
	}

    @Test
	public void testRegistryContentForTestCaseBehavior() {
		Assert.assertTrue("TestCase Behavior element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.TestCase_Behavior"));
	}

    @Test
	public void testRegistryContentForTestCaseOperation() {
		Assert.assertTrue("TestCase Operation element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.TestCase_Operation"));
	}

    @Test
	public void testRegistryContentForTraceAbstraction() {
		Assert.assertTrue("Trace Abstraction element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Trace_Abstraction"));
	}

    @Test
	public void testRegistryContentForTraceDirectedRelationship() {
		Assert.assertTrue("Trace DirectedRelationship element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Trace_DirectedRelationship"));
	}

    @Test
	public void testRegistryContentForTriggerOnNestedPortElement() {
		Assert.assertTrue("TriggerOnNestedPort Element element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.TriggerOnNestedPort_Element"));
	}

    @Test
	public void testRegistryContentForTriggerOnNestedPortTrigger() {
		Assert.assertTrue("TriggerOnNestedPort Trigger element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.TriggerOnNestedPort_Trigger"));
	}

    @Test
	public void testRegistryContentForValueType() {
		Assert.assertTrue("ValueType element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.ValueType"));
	}

    @Test
	public void testRegistryContentForVerifyAbstraction() {
		Assert.assertTrue("Verify Abstraction element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Verify_Abstraction"));
	}

    @Test
	public void testRegistryContentForVerifyDirectedRelationship() {
		Assert.assertTrue("Verify DirectedRelationship element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Verify_DirectedRelationship"));
	}

    @Test
	public void testRegistryContentForView() {
		Assert.assertTrue("View element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.View"));
	}

    @Test
	public void testRegistryContentForViewpoint() {
		Assert.assertTrue("Viewpoint element type not found in SysML element type set ", SysMLServiceTypeUtil.isKnown("org.eclipse.papyrus.SysML14.Viewpoint"));
	}



}
