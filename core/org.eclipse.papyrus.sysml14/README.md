# SysML1.4
A Papyrus DSML for the SysML 1.4 OMG norm

### Module structure ###

src-gen : generated code from the SysML profile
src : java code (mostly for derived properties)
doc/omg.sysml14.autoreq.tsv: list of requirements extracted from the normative pdf
doc/omg.sysml.{uml,notation,di}: model with the auto extracted requirements
doc/org.eclipse.papyrus.sysml14.{uml,notation,di}: documentation model for the profile (Requirements,TestCase ...)
doc/SysML-Extension.{uml,notation,di}: model to track all addition done to the official SysML profile
resources/icons : icon for SysML profile
resources/library/QUDV.{uml,notation,di}: official OMG QUDV library (modified to be eclipse compliant)
resources/library/SysML-Standard-Library.{uml,notation,di}: library extracted from official OMG SysML profile (non Stereotype elements) 
resources/profile/SysML.profile.{uml,notation,di}: official OMG SysML profile (modified to be eclipse compliant)