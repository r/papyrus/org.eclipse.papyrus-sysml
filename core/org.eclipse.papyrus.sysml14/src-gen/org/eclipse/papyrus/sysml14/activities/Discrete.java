/**
 * Copyright (c) 2015 CEA LIST.
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 * 
 */
package org.eclipse.papyrus.sysml14.activities;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Discrete</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Discrete rate is a special case of rate of flow (see Rate) where the increment of time between items is non-zero.
 * <!-- end-model-doc -->
 *
 *
 * @see org.eclipse.papyrus.sysml14.activities.ActivitiesPackage#getDiscrete()
 * @model
 * @generated
 */
public interface Discrete extends Rate {
} // Discrete
