/**
 * Copyright (c) 2015 CEA LIST.
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 * 
 */
package org.eclipse.papyrus.sysml14.portsandflows.internal.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.sysml14.blocks.internal.impl.BlockImpl;

import org.eclipse.papyrus.sysml14.portsandflows.InterfaceBlock;
import org.eclipse.papyrus.sysml14.portsandflows.PortsandflowsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InterfaceBlockImpl extends BlockImpl implements InterfaceBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfaceBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortsandflowsPackage.Literals.INTERFACE_BLOCK;
	}

} //InterfaceBlockImpl
