/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.constraintblocks;
import org.eclipse.papyrus.sysml14.constraintblocks.internal.impl.ConstraintblocksFactoryImpl;


/**
 * 
 *
 */
public class ConstraintBlocksFactoryCustomImpl extends ConstraintblocksFactoryImpl implements ConstraintblocksFactory{
	
	/**
	 * @see org.eclipse.papyrus.sysml14.constraintblocks.internal.impl.ConstraintblocksFactoryImpl#createConstraintBlock()
	 *
	 * @return the BoundReference
	 */
	@Override
	public ConstraintBlock createConstraintBlock() {
		return new ConstraintBlockCustomImpl();
	}
	
}
