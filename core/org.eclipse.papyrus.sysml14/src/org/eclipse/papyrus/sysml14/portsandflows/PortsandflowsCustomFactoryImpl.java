/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.portsandflows;

import org.eclipse.papyrus.sysml14.portsandflows.internal.impl.PortsandflowsFactoryImpl;

/**
 *
 *
 */
public class PortsandflowsCustomFactoryImpl extends PortsandflowsFactoryImpl {

	
	/**
	 * @see org.eclipse.papyrus.sysml14.portsandflows.internal.impl.PortsandflowsFactoryImpl#createFlowProperty()
	 *
	 * @return
	 */
	@Override
	public FlowProperty createFlowProperty() {
		return new FlowPropertyCustomImpl();
	}
}
