/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.validation.rules.portsandflows;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.sysml14.portsandflows.InvocationOnNestedPortAction;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InvocationAction;

/**
 * 9.3.2.10 InvocationOnNestedPortAction [5] InvocationOnNestedPortAction may
 * only be applied to invocation actions.
 */
public class InvocationOnNestedPortActionOnlyInvocationActionModelConstraint extends AbstractModelConstraint {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.validation.AbstractModelConstraint#validate(org.eclipse.
	 * emf.validation.IValidationContext)
	 */
	@Override
	public IStatus validate(IValidationContext context) {
		InvocationOnNestedPortAction invocationOnNestedPortAction = (InvocationOnNestedPortAction) context.getTarget();
		Element element = invocationOnNestedPortAction.getBase_Element();
		if (element != null && !(element instanceof InvocationAction)) {
			return context.createFailureStatus(context.getTarget());
		}
		return context.createSuccessStatus();
	}

}
