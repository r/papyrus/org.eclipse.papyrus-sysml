/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.validation.rules.requirements;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.sysml14.requirements.Trace;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.NamedElement;
/**
 * 
 * 16.3.2.6 Trace  (SysML 1.4)
 * [1]The supplier must be an element stereotyped by «requirement» or one of «requirement» subtypes.
 *
 */
public class TraceOneClientSupplierModelConstraint extends AbstractModelConstraint {


	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.AbstractModelConstraint#validate(org.eclipse.emf.validation.IValidationContext)
	 */
	@Override
	public IStatus validate(IValidationContext context) {
		Trace trace = (Trace) context.getTarget();
		Abstraction abstraction = trace.getBase_Abstraction();
		if (abstraction == null) {
			return context.createFailureStatus(context.getTarget());
		}
		EList<NamedElement> clients = abstraction.getClients();
		EList<NamedElement> suppliers = abstraction.getSuppliers();
		if (clients == null || clients.size()!= 1 || suppliers == null || suppliers.size() != 1) {
			return context.createFailureStatus(context.getTarget());
		}		
		return context.createSuccessStatus();
	}

}
