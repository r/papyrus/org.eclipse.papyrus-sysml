/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.validation.rules.allocations;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.sysml14.allocations.Allocate;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.DirectedRelationship;

/**
 * 15.3.2.1 Allocate[1] The Allocate stereotype may only be applied to
 * abstractions.
 */
public class AllocateOnlyAbstractionModelConstraint extends AbstractModelConstraint {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.validation.AbstractModelConstraint#validate(org.eclipse.
	 * emf.validation.IValidationContext)
	 */
	@Override
	public IStatus validate(IValidationContext context) {
		Allocate allocate = (Allocate) context.getTarget();

		DirectedRelationship directedRelationship = allocate.getBase_DirectedRelationship();
		if (directedRelationship != null && !(directedRelationship instanceof Abstraction)) {
			return context.createFailureStatus(context.getTarget());
		}

		Abstraction abstraction = allocate.getBase_Abstraction();
		if (abstraction == null) {
			return context.createFailureStatus(context.getTarget());
		}

		return context.createSuccessStatus();
	}

}
