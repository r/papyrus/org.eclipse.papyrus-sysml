/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.validation.selectors;

import org.eclipse.emf.validation.model.IClientSelector;



/**
 * Temporary solution to execute validation on uml context
 * @deprecated
 */
@Deprecated // to remove once Bug 507734 is solved
public class TrueSelector implements IClientSelector {

	@Override
	public boolean selects(Object object) {
		return true;
	}
}
