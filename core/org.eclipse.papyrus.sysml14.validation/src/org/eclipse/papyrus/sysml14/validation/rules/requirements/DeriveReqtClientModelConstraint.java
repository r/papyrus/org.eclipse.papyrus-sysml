/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *   
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.validation.rules.requirements;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.sysml14.requirements.DeriveReqt;
import org.eclipse.papyrus.sysml14.requirements.Requirement;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;
/**
 * 16.3.2.2 DeriveReqt (SysML 1.4)
 * Constraints
 * [2] The client must be an element stereotyped by «requirement» or one of «requirement» subtypes.
 */
//TODO : I reported an issue since the constraint is not "correct" in SysML 1.4 http://issues.omg.org/browse/SYSMLR-240
public class DeriveReqtClientModelConstraint extends AbstractModelConstraint {

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.AbstractModelConstraint#validate(org.eclipse.emf.validation.IValidationContext)
	 */
	@Override
	public IStatus validate(IValidationContext context) {
		DeriveReqt deriveReqt = (DeriveReqt) context.getTarget();
		Abstraction abstraction = deriveReqt.getBase_Abstraction();
		if (abstraction != null){
			EList<NamedElement> clients = abstraction.getClients();
			if (clients != null && !clients.isEmpty()){
				if(UMLUtil.getStereotypeApplication(clients.get(0), Requirement.class)==null){
					return context.createFailureStatus(context.getTarget());
				}
			}
		}
		return context.createSuccessStatus();
	}

}
