/*****************************************************************************
 * Copyright (c) 2010 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.service.types.matcher;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.IElementMatcher;
import org.eclipse.papyrus.sysml14.deprecatedelements.FlowPort;
import org.eclipse.papyrus.sysml14.portsandflows.FlowDirection;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Test if current {@link Port} is a {@link FlowPort} with INOUT direction
 */
public class FlowPortInOutMatcher implements IElementMatcher {

	/**
	 * Test if the parameter match a FlowPortINOut, so if it :
	 *  - is a Uml Port
	 *  - has the Flow Port stereotype applied
	 *  - the direction of the applied FlowPort is INOUT
	 * 
	 * @see org.eclipse.gmf.runtime.emf.type.core.IElementMatcher#matches(org.eclipse.emf.ecore.EObject)
	 *
	 * @param eObject
	 * @return
	 */
	@Override
	public boolean matches(EObject eObject) {
		boolean isFlowPortInOut = false;
		if (eObject instanceof Port) {
			Port port = (Port) eObject;
			FlowPort flowPort = UMLUtil.getStereotypeApplication(port, FlowPort.class);
			if ((flowPort != null) && (flowPort.getDirection() == FlowDirection.INOUT)) {
				isFlowPortInOut = true;
			}
		}
		return isFlowPortInOut;
	}
}
