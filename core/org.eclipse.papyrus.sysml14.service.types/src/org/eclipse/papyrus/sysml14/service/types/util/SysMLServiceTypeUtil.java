/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.service.types.util;

import org.eclipse.gmf.runtime.emf.type.core.ClientContextManager;
import org.eclipse.gmf.runtime.emf.type.core.IClientContext;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditServiceProvider;

/**
 * List all SysML 1.4 element type ids
 */
public final class SysMLServiceTypeUtil {

	/**
	 * Say if an element is know in the SysML 1.4 context
	 * @param elementTypeId
	 * @return
	 */
	public static boolean isKnown(String elementTypeId) {
		IClientContext context = ClientContextManager.getInstance().getClientContext(SYSML14_CONTEXT_ID);
		IElementEditServiceProvider editServiceProvider = ElementEditServiceUtils.getEditServiceProvider(context);
		return editServiceProvider.isKnownElementType(elementTypeId);
	}

	public static final String SYSML14_CONTEXT_ID = "org.eclipse.papyrus.infra.services.edit.TypeContext"; //$NON-NLS-1$
			//"org.eclipse.papyrus.sysml.architecture.SysML14"; //$NON-NLS-1$

	
	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_VALUETYPE = "org.eclipse.papyrus.SysML14.ValueType"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_DISTRIBUTEDPROPERTY = "org.eclipse.papyrus.SysML14.DistributedProperty"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CONNECTORPROPERTY = "org.eclipse.papyrus.SysML14.ConnectorProperty"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_PARTICIPANTPROPERTY = "org.eclipse.papyrus.SysML14.ParticipantProperty"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_BINDINGCONNECTOR = "org.eclipse.papyrus.SysML14.BindingConnector"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_BLOCK = "org.eclipse.papyrus.SysML14.Block"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_PROPERTYSPECIFICTYPE = "org.eclipse.papyrus.SysML14.PropertySpecificType"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_NESTEDCONNECTOREND_CONNECTOREND = "org.eclipse.papyrus.SysML14.NestedConnectorEnd_ConnectorEnd"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_NESTEDCONNECTOREND_ELEMENT = "org.eclipse.papyrus.SysML14.NestedConnectorEnd_Element"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_DIRECTEDRELATIONSHIPPROPERTYPATH = "org.eclipse.papyrus.SysML14.DirectedRelationshipPropertyPath"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_ELEMENTPROPERTYPATH = "org.eclipse.papyrus.SysML14.ElementPropertyPath"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_ENDPATHMULTIPLICITY = "org.eclipse.papyrus.SysML14.EndPathMultiplicity"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_BOUNDREFERENCE = "org.eclipse.papyrus.SysML14.BoundReference"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_ADJUNCTPROPERTY = "org.eclipse.papyrus.SysML14.AdjunctProperty"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CLASSIFIERBEHAVIORPROPERTY = "org.eclipse.papyrus.SysML14.ClassifierBehaviorProperty"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_ITEMFLOW = "org.eclipse.papyrus.SysML14.ItemFlow"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_FLOWPROPERTY = "org.eclipse.papyrus.SysML14.FlowProperty"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_FULLPORT = "org.eclipse.papyrus.SysML14.FullPort"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_INTERFACEBLOCK = "org.eclipse.papyrus.SysML14.InterfaceBlock"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_PROXYPORT = "org.eclipse.papyrus.SysML14.ProxyPort"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_ACCEPTCHANGESTRUCTURALFEATUREEVENTACTION = "org.eclipse.papyrus.SysML14.AcceptChangeStructuralFeatureEventAction"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CHANGESTRUCTURALFEATUREEVENT = "org.eclipse.papyrus.SysML14.ChangeStructuralFeatureEvent"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_DIRECTEDFEATURE = "org.eclipse.papyrus.SysML14.DirectedFeature"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_INVOCATIONONNESTEDPORTACTION_INVOCATIONACTION = "org.eclipse.papyrus.SysML14.InvocationOnNestedPortAction_InvocationAction"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_INVOCATIONONNESTEDPORTACTION_ELEMENT = "org.eclipse.papyrus.SysML14.InvocationOnNestedPortAction_Element"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_TRIGGERONNESTEDPORT_TRIGGER = "org.eclipse.papyrus.SysML14.TriggerOnNestedPort_Trigger"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_TRIGGERONNESTEDPORT_ELEMENT = "org.eclipse.papyrus.SysML14.TriggerOnNestedPort_Element"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_OVERWRITE = "org.eclipse.papyrus.SysML14.Overwrite"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CONTROLOPERATOR_BEHAVIOR = "org.eclipse.papyrus.SysML14.ControlOperator_Behavior"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CONTROLOPERATOR_OPERATION = "org.eclipse.papyrus.SysML14.ControlOperator_Operation"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_RATE_ACTIVITYEDGE = "org.eclipse.papyrus.SysML14.Rate_ActivityEdge"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_RATE_PARAMETER = "org.eclipse.papyrus.SysML14.Rate_Parameter"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_NOBUFFER = "org.eclipse.papyrus.SysML14.NoBuffer"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_DISCRETE_ACTIVITYEDGE = "org.eclipse.papyrus.SysML14.Discrete_ActivityEdge"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_DISCRETE_PARAMETER = "org.eclipse.papyrus.SysML14.Discrete_Parameter"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CONTINUOUS_ACTIVITYEDGE = "org.eclipse.papyrus.SysML14.Continuous_ActivityEdge"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CONTINUOUS_PARAMETER = "org.eclipse.papyrus.SysML14.Continuous_Parameter"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_PROBABILITY_ACTIVITYEDGE = "org.eclipse.papyrus.SysML14.Probability_ActivityEdge"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_PROBABILITY_PARAMETERSET = "org.eclipse.papyrus.SysML14.Probability_ParameterSet"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_OPTIONAL = "org.eclipse.papyrus.SysML14.Optional"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_RATIONALE = "org.eclipse.papyrus.SysML14.Rationale"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_VIEW = "org.eclipse.papyrus.SysML14.View"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CONFORM = "org.eclipse.papyrus.SysML14.Conform"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_PROBLEM = "org.eclipse.papyrus.SysML14.Problem"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_VIEWPOINT = "org.eclipse.papyrus.SysML14.Viewpoint"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_STAKEHOLDER = "org.eclipse.papyrus.SysML14.Stakeholder"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_EXPOSE = "org.eclipse.papyrus.SysML14.Expose"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_ELEMENTGROUP = "org.eclipse.papyrus.SysML14.ElementGroup"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CONSTRAINTBLOCK = "org.eclipse.papyrus.SysML14.ConstraintBlock"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_ALLOCATEACTIVITYPARTITION = "org.eclipse.papyrus.SysML14.AllocateActivityPartition"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_ALLOCATE_ABSTRACTION = "org.eclipse.papyrus.SysML14.Allocate_Abstraction"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_ALLOCATE_DIRECTEDRELATIONSHIP = "org.eclipse.papyrus.SysML14.Allocate_DirectedRelationship"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_DERIVEREQT_ABSTRACTION = "org.eclipse.papyrus.SysML14.DeriveReqt_Abstraction"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_DERIVEREQT_DIRECTEDRELATIONSHIP = "org.eclipse.papyrus.SysML14.DeriveReqt_DirectedRelationship"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_COPY_ABSTRACTION = "org.eclipse.papyrus.SysML14.Copy_Abstraction"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_COPY_DIRECTEDRELATIONSHIP = "org.eclipse.papyrus.SysML14.Copy_DirectedRelationship"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_SATISFY_ABSTRACTION = "org.eclipse.papyrus.SysML14.Satisfy_Abstraction"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_SATISFY_DIRECTEDRELATIONSHIP = "org.eclipse.papyrus.SysML14.Satisfy_DirectedRelationship"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_TESTCASE_OPERATION = "org.eclipse.papyrus.SysML14.TestCase_Operation"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_TESTCASE_BEHAVIOR = "org.eclipse.papyrus.SysML14.TestCase_Behavior"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_REQUIREMENT = "org.eclipse.papyrus.SysML14.Requirement"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_VERIFY_ABSTRACTION = "org.eclipse.papyrus.SysML14.Verify_Abstraction"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_VERIFY_DIRECTEDRELATIONSHIP = "org.eclipse.papyrus.SysML14.Verify_DirectedRelationship"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_TRACE_ABSTRACTION = "org.eclipse.papyrus.SysML14.Trace_Abstraction"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_TRACE_DIRECTEDRELATIONSHIP = "org.eclipse.papyrus.SysML14.Trace_DirectedRelationship"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_REFINE_ABSTRACTION = "org.eclipse.papyrus.SysML14.Refine_Abstraction"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_REFINE_DIRECTEDRELATIONSHIP = "org.eclipse.papyrus.SysML14.Refine_DirectedRelationship"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_FLOWPORT = "org.eclipse.papyrus.SysML14.FlowPort"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_FLOWSPECIFICATION = "org.eclipse.papyrus.SysML14.FlowSpecification"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_UNIT = "org.eclipse.papyrus.SysML14.Unit"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_QUANTITYKIND = "org.eclipse.papyrus.SysML14.QuantityKind"; //$NON-NLS-1$

	// List here the elementype present in the extension element type set

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CONSTRAINTPROPERTY = "org.eclipse.papyrus.SysML14.ConstraintProperty"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_REFERENCE = "org.eclipse.papyrus.SysML14.Reference"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_PART = "org.eclipse.papyrus.SysML14.Part"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_VALUE = "org.eclipse.papyrus.SysML14.Value"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_ACTORPART = "org.eclipse.papyrus.SysML14.ActorPart"; //$NON-NLS-1$

	private SysMLServiceTypeUtil() {
	}
}
