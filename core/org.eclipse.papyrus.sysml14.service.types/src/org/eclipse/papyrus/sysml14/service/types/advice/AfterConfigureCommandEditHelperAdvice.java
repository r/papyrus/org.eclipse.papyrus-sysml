/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.service.types.advice;

import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;

/**
 * The EditHelperAdvice class used for:
 * The property value can imply to launch a popup
 * this class satisfies the requirement{@papyrus.req org.eclipse.papyrus.sysml14.service.types#req_010_002}
 *
 * The reference can imply to launch a popup
 * this class satisfies the requirement{@papyrus.req org.eclipse.papyrus.sysml14.service.types#req_010_003}
 *
 * The part can imply to launch a popup
 * this class satisfies the requirement{@papyrus.req org.eclipse.papyrus.sysml14.service.types#req_010_004}
 *
 * The flowproperty value can imply to launch a popup
 * this class satisfies the requirement{@papyrus.req org.eclipse.papyrus.sysml14.service.types#req_010_005}
 */
public class AfterConfigureCommandEditHelperAdvice extends AbstractEditHelperAdvice {


	/**
	 * Parameter id used to pass the command for setting the type of the property
	 */
	public static final String AFTER_CONFIGURE_COMMAND = "AFTER_CONFIGURE_COMMAND"; //$NON-NLS-1$


	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest)
	 *
	 * @param request
	 * @return
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		Object parameter = request.getParameter(AFTER_CONFIGURE_COMMAND);

		if (parameter instanceof ICommand) {
			return (ICommand) parameter;
		}
		return super.getAfterConfigureCommand(request);
	}
}
