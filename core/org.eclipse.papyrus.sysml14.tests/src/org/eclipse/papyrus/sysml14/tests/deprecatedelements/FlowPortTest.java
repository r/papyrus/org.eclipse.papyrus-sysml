/*****************************************************************************
 * Copyright (c) 2009, 2014 CEA LIST and others.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Yann Tanguy (CEA LIST) yann.tanguy@cea.fr - Initial API and implementation
 *  Christian W. Damus (CEA) - bug 422257
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Transcode the test to SysML 1.4
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.tests.deprecatedelements;

import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.sysml14.deprecatedelements.DeprecatedelementsPackage;
import org.eclipse.papyrus.sysml14.deprecatedelements.FlowPort;
import org.eclipse.papyrus.sysml14.deprecatedelements.FlowPortCustomImpl;
import org.eclipse.papyrus.sysml14.portsandflows.FlowDirection;
import org.eclipse.papyrus.sysml14.util.DeprecatedElementUtil;
import org.eclipse.papyrus.sysml14.util.SysMLResource;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Image;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.util.UMLUtil.StereotypeApplicationHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * A test case for the model object '
 * <em><b>Flow Port</b></em>'.
 * <p>
 * The following features are tested:
 * <ul>
 * <li>{@link org.eclipse.papyrus.sysml14.deprecatedelements.FlowPort#isAtomic()
 * <em>Is Atomic</em>}</li>
 * </ul>
 * </p>
 * <p>
 * The following operations are tested:
 * <ul>
 * <li>{@link org.eclipse.papyrus.sysml14.deprecatedelements.FlowPort#getIcon()
 * <em>Get Icon</em>}</li>
 * </ul>
 * </p>
 *
 */
@SuppressWarnings("nls")
public class FlowPortTest {

	private Port p0 = null;

	private FlowPort fpDefault = null;

	private FlowPort fpOut = null;

	private FlowPort fpIn = null;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp() {
		// Prepare test
		Model model = SysMLResource.createSysMLModel(new ResourceSetImpl());		
		Assert.assertFalse("the SysML profil must be applied.", model.getAppliedProfiles().isEmpty());

		// Create classes & interface
		Class c0 = model.createOwnedClass("c0", false);
		Interface i2 = model.createOwnedInterface("i2");

		// Create type
		PrimitiveType ptype = model.createOwnedPrimitiveType("type");

		// Create ports
		p0 = c0.createOwnedPort("p0", null);
		Port p1 = c0.createOwnedPort("p1", ptype);
		Port p2 = c0.createOwnedPort("p2", i2); //$NON-NLS-1$

		// Add FlowPort stereotypes
		StereotypeApplicationHelper stereotypeApplicationHelper = StereotypeApplicationHelper.getInstance(null);
		fpDefault = (FlowPort) stereotypeApplicationHelper.applyStereotype(p0,
				DeprecatedelementsPackage.eINSTANCE.getFlowPort(),null);

		fpOut = (FlowPort) stereotypeApplicationHelper.applyStereotype(p1,
				DeprecatedelementsPackage.eINSTANCE.getFlowPort(),null);
		fpOut.setDirection(FlowDirection.OUT);
		fpIn = (FlowPort) stereotypeApplicationHelper.applyStereotype(p2,
				DeprecatedelementsPackage.eINSTANCE.getFlowPort(),null);
		fpIn.setDirection(FlowDirection.IN);

		p2.setIsConjugated(true);

		// Add FlowSpecification
		stereotypeApplicationHelper.applyStereotype(i2, DeprecatedelementsPackage.eINSTANCE.getFlowSpecification(),null);
	}

	/**
	 * Tests the '
	 * {@link org.eclipse.papyrus.sysml14.deprecatedelements.FlowPort#isAtomic()
	 * <em>Is Atomic</em>}' feature getter.
	 *
	 * @see org.eclipse.papyrus.sysml14.deprecatedelements.FlowPort#isAtomic()
	 */
	@Test
	public void testIsAtomic() {		
		Assert.assertTrue("FlowPort without type is Atomic", fpDefault.isAtomic());
		Assert.assertTrue("FlowPort type is not a FlowSpecification",fpOut.isAtomic());
		Assert.assertFalse("FlowPort type is a FlowSpecification", fpIn.isAtomic());
	}

	/**
	 * Tests the '
	 * {@link org.eclipse.papyrus.sysml14.deprecatedelements.FlowPort#getIcon() <em>Get Icon</em>}' operation. 
	 *
	 * @see org.eclipse.papyrus.sysml14.deprecatedelements.FlowPort#getIcon()
	 * 
	 */
	@Test
	public void testGetIcon() {
		Image imageIn = FlowPortCustomImpl.getIcons(fpIn).get("FlowPort_IN"); 
		Image imageOut = FlowPortCustomImpl.getIcons(fpOut).get("FlowPort_OUT"); 
		Image imageInOut = FlowPortCustomImpl.getIcons(fpDefault).get("FlowPort_INOUT"); 
				
		Assert.assertEquals("A Flow property with an INOUT direction must have the INOUT image",imageInOut,fpDefault.getIcon());
		Assert.assertEquals("A Flow property with an OUT direction must have the OUT image",imageOut,fpOut.getIcon());
		Assert.assertEquals("A Flow property with an IN direction must have the IN image",imageIn,fpIn.getIcon());
	}
	
	/**
	 * Tests the '
	 * {@link org.eclipse.papyrus.sysml14.util.DeprecatedElementUtil#isDeprecatedElement()
	 * <em>Is Deprecated</em>}' feature getter.
	 *
	 * @see org.eclipse.papyrus.sysml14.util.DeprecatedElementUtil#isDeprecatedElement()
	 */
	@Test
	public void testIsDeprecated() {
		Assert.assertTrue("FlowPort is deprecated", DeprecatedElementUtil.isDeprecatedElement(p0));
		Assert.assertTrue("FlowPort is deprecated", DeprecatedElementUtil.isDeprecatedElement(fpOut));
	}
}
