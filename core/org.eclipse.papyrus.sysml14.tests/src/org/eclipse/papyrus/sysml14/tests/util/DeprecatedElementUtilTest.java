/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *  
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.tests.util;

import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.sysml14.blocks.Block;
import org.eclipse.papyrus.sysml14.blocks.BlocksPackage;
import org.eclipse.papyrus.sysml14.deprecatedelements.DeprecatedelementsPackage;
import org.eclipse.papyrus.sysml14.deprecatedelements.FlowPort;
import org.eclipse.papyrus.sysml14.deprecatedelements.FlowSpecification;
import org.eclipse.papyrus.sysml14.util.DeprecatedElementUtil;
import org.eclipse.papyrus.sysml14.util.SysMLResource;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.util.UMLUtil.StereotypeApplicationHelper;
import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for deprecated utility class
 * 
 */
@SuppressWarnings("nls")
public class DeprecatedElementUtilTest {

	/**
	 * Check that FlowPort and FlowSpecification are deprecated
	 */
	@Test
	public void testIsDeprecatedElement()  {
		// Prepare Model test
		Model model = SysMLResource.createSysMLModel(new ResourceSetImpl());
		Assert.assertFalse("the SysML profil must be applied.", model.getAppliedProfiles().isEmpty());

		StereotypeApplicationHelper stereotypeApplicationHelper = StereotypeApplicationHelper.getInstance(null);
		Class c0 = model.createOwnedClass("class", false);

		// Create ports
		Port port = c0.createOwnedPort("port", null);

		FlowPort fpDefault = (FlowPort) stereotypeApplicationHelper.applyStereotype(port,
				DeprecatedelementsPackage.eINSTANCE.getFlowPort(),null);

		Assert.assertTrue("FlowPort should be deprecated", DeprecatedElementUtil.isDeprecatedElement(fpDefault));
		Assert.assertTrue("FlowPort should be deprecated", DeprecatedElementUtil.isDeprecatedElement(port));
		
		Interface flowSpecificationInterface=model.createOwnedInterface("FlowSpecification1");
		FlowSpecification flowSpecification = (FlowSpecification) stereotypeApplicationHelper.applyStereotype(flowSpecificationInterface, DeprecatedelementsPackage.eINSTANCE.getFlowSpecification(),null);
		Assert.assertTrue("FlowSpecification should be deprecated", DeprecatedElementUtil.isDeprecatedElement(flowSpecificationInterface));
		Assert.assertTrue("FlowSpecification should be deprecated", DeprecatedElementUtil.isDeprecatedElement(flowSpecification));
		
		Class blockClass=model.createOwnedClass("Block1", false);
		Block block = (Block) stereotypeApplicationHelper.applyStereotype(blockClass, BlocksPackage.eINSTANCE.getBlock(),null);
		Assert.assertFalse("Class should not be deprecated", DeprecatedElementUtil.isDeprecatedElement(blockClass));
		Assert.assertFalse("Block should not be deprecated", DeprecatedElementUtil.isDeprecatedElement(block));		
		
	}

}
