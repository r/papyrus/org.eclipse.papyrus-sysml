/*****************************************************************************
 * Copyright (c) 2015,2018 CEA LIST.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.tests;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.Collection;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.uml.UMLPlugin;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Basic test for SysML 1.4 profile
 * 
 * @author Benoit Maggi
 */
@SuppressWarnings("nls")
@RunWith(Parameterized.class)
public class SysmlProfileTest {

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { 
				{ "http://www.eclipse.org/papyrus/sysml/1.4/SysML"},
				{ "http://www.eclipse.org/papyrus/sysml/1.4/SysML/Activities"},
				{ "http://www.eclipse.org/papyrus/sysml/1.4/SysML/Allocations"},
				{ "http://www.eclipse.org/papyrus/sysml/1.4/SysML/Blocks"},
				{ "http://www.eclipse.org/papyrus/sysml/1.4/SysML/ConstraintBlocks"},
				{ "http://www.eclipse.org/papyrus/sysml/1.4/SysML/DeprecatedElements"},
				{ "http://www.eclipse.org/papyrus/sysml/1.4/SysML/PortsAndFlows"},
				{ "http://www.eclipse.org/papyrus/sysml/1.4/SysML/ModelElements"},
				{ "http://www.eclipse.org/papyrus/sysml/1.4/SysML/Requirements"}
		});
	}

	private String uri;

	public SysmlProfileTest(String uri) {
		this.uri = uri;
	}

	/**
	 * Check that we have a location for the uri
	 * and that the location point to some existing EObject
	 */
	@Test
	public void testProfilRegistration() {
		URI location = UMLPlugin.getEPackageNsURIToProfileLocationMap().get(uri);
		assertNotNull("Location missing for this uri :"+uri, location);
		assertNotNull("Location is refering to a non existing EObject :"+location, new ResourceSetImpl().getEObject(location,true));
	}

}
