/*****************************************************************************
 * Copyright (c) 2009, 2014 CEA LIST and others.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Patrick Tessier (CEA LIST)Patrick.tessier@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.tests.modelelements;



import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.sysml14.modelelements.ModelelementsPackage;
import org.eclipse.papyrus.sysml14.modelelements.Stakeholder;
import org.eclipse.papyrus.sysml14.util.SysMLResource;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.util.UMLUtil.StereotypeApplicationHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * A test case for the model object '<em><b>Stakeholder</b></em>'.
 *  See the requirement <b>{@papyrus.req org.eclipse.papyrus.sysml14#Req012}</b>. 
 * <p>
 * The following features are tested:
 * <ul>
 * <li>{@link org.eclipse.papyrus.sysml14.modelelements.StakeholderTest#testGetConcern <em>Concern</em>}</li>
 * </ul>
 * </p>
 */
@SuppressWarnings("nls")
public class StakeholderTest {

	private static final String THIS_IS_THE_BODY_OF_COMMENT2 = "this is the body of comment2";

	private static final String THIS_IS_THE_BODY_OF_COMMENT1 = "this is the body of comment1";

	private Stakeholder defaultStakeholder= null;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp()  {
		// Prepare test
		Model model = SysMLResource.createSysMLModel(new ResourceSetImpl());
		
		Assert.assertFalse("the SysML profil must be applied.", model.getAppliedProfiles().isEmpty());

		// Create classes & interface
		Class defaultClass = model.createOwnedClass("defaultClass", false);

		// Add stakeholder stereotypes
		defaultStakeholder = (Stakeholder)StereotypeApplicationHelper.getInstance(null).applyStereotype(defaultClass, ModelelementsPackage.eINSTANCE.getStakeholder(),null);
		Comment comment1=UMLFactory.eINSTANCE.createComment();
		comment1.setBody(THIS_IS_THE_BODY_OF_COMMENT1);
		model.getOwnedComments().add(comment1);
		Comment comment2=UMLFactory.eINSTANCE.createComment();
		comment2.setBody(THIS_IS_THE_BODY_OF_COMMENT2);
		model.getOwnedComments().add(comment2);
		defaultStakeholder.getConcernList().add(comment1);
		defaultStakeholder.getConcernList().add(comment2);
	}


	/**
	 * Tests the '{@link org.eclipse.papyrus.sysml14.modelelements.Stakeholder#getConcern()
	 * <em>concern</em>}' feature getter. 
	 * @see org.eclipse.papyrus.sysml14.modelelements.Stakeholder#getConcern()
	 */
	@Test
	public void testGetConcern() {
		Assert.assertEquals("The derived property concern is badly calculated, the size of the list must be 2.", 2, defaultStakeholder.getConcern().size());
		Assert.assertEquals("The derived property concern is badly calculated, the body of the first comment is not at the first place",THIS_IS_THE_BODY_OF_COMMENT1,defaultStakeholder.getConcern().get(0));
		Assert.assertEquals("The derived property concern is badly calculated, the body of the second comment is not à the second place",THIS_IS_THE_BODY_OF_COMMENT2,defaultStakeholder.getConcern().get(1));
	}

}
