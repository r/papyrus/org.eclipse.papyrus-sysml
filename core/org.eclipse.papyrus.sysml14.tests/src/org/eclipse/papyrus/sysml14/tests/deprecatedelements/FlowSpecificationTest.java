/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Francois Le Fevre (CEA LIST) francois.le-fevre@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.tests.deprecatedelements;

import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.sysml14.deprecatedelements.DeprecatedelementsPackage;
import org.eclipse.papyrus.sysml14.deprecatedelements.FlowSpecification;
import org.eclipse.papyrus.sysml14.portsandflows.PortsandflowsPackage;
import org.eclipse.papyrus.sysml14.util.SysMLResource;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil.StereotypeApplicationHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * A test case for the model object '<em><b>FlowSpecification</b></em>'.
 * 
 * <p>
 * The following features are tested:
 * <ul>
 * <li>{@link org.eclipse.papyrus.sysml14.deprecatedelements.FlowSpecification#getFlowProperties <em>FlowProperties</em>}</li>
 * </ul>
 * </p>
 */
@SuppressWarnings("nls")
public class FlowSpecificationTest {

	private FlowSpecification flowSpecification=null;

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp()  {
		// Prepare Model test
		Model model = SysMLResource.createSysMLModel(new ResourceSetImpl());
		Assert.assertFalse("the SysML profil must be applied.", model.getAppliedProfiles().isEmpty());

		StereotypeApplicationHelper stereotypeApplicationHelper = StereotypeApplicationHelper.getInstance(null);
		/*
		 * create a FlowSpecification
		 */
		Interface flowSpecificationInterface=model.createOwnedInterface("FlowSpecification1");
		flowSpecification = (FlowSpecification) stereotypeApplicationHelper.applyStereotype(flowSpecificationInterface, DeprecatedelementsPackage.eINSTANCE.getFlowSpecification(),null);

		/*
		 * create a flow property and add it to the FlowSpecification
		 */
		Property property1 = flowSpecificationInterface.createOwnedAttribute("property1", null);
		StereotypeApplicationHelper.getInstance(null).applyStereotype(property1, PortsandflowsPackage.eINSTANCE.getFlowProperty(),null);
		
	}
	
	/**
	 * @pap.req Flow specifications cannot own operations or receptions (they can only own FlowProperties)
	 */
	@Test
	public void testGetOperations() { 
		Assert.assertEquals("Operations size should be 0", 0, flowSpecification.getBase_Interface().getAllOperations().size());
	}
	
	/**
	 * @pap.req Flow specifications cannot own operations or receptions (they can only own FlowProperties)
	 */
	@Test
	public void testGetReceptions() { 
		Assert.assertEquals("Receptions size should be 0", 0, flowSpecification.getBase_Interface().getOwnedReceptions().size());
	}

	/**
	 * Tests the '{@link org.eclipse.papyrus.sysml14.deprecatedelements.FlowSpecification#getFlowProperties()
	 * <em>FlowProperty</em>}' feature getter. 
	 * @see org.eclipse.papyrus.sysml14.deprecatedelements.FlowSpecification#getFlowProperties()
	 */
	@Test
	public void testGetFlowProperties() {  
		Assert.assertNotNull("FlowProperties should not be null", flowSpecification.getFlowProperties());
		Assert.assertEquals("FlowProperties size should be 1", 1, flowSpecification.getFlowProperties().size());
	}
	


} 
