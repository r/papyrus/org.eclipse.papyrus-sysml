/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Francois Le Fevre (CEA LIST) francois.le-fevre@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.tests.constraintblocks;

import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.sysml14.blocks.BlocksPackage;
import org.eclipse.papyrus.sysml14.constraintblocks.ConstraintBlock;
import org.eclipse.papyrus.sysml14.constraintblocks.ConstraintblocksPackage;
import org.eclipse.papyrus.sysml14.util.SysMLResource;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.util.UMLUtil.StereotypeApplicationHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * A test case for the model object '<em><b>Block</b></em>'.
 * 
 * <p>
 * The following features are tested:
 * <ul>
 * <li>{@link org.eclipse.papyrus.sysml14.constraintblocks.ConstraintBlock#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 */
@SuppressWarnings("nls")
public class ConstraintBlockTest {

	private ConstraintBlock constraintBlock=null; 

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Before
	public void setUp()  {
		// Prepare Model test
		Model model = SysMLResource.createSysMLModel(new ResourceSetImpl());
		Assert.assertFalse("the SysML profil must be applied.", model.getAppliedProfiles().isEmpty());

		StereotypeApplicationHelper stereotypeApplicationHelper = StereotypeApplicationHelper.getInstance(null);

/*		Constraint blocks support a special form of compartment, with the label “parameters,” which may contain declarations for
		some or all of its constraint parameters. Properties of a constraint block should be shown either in the constraints
		compartment, for org eclipse uml2 create nested constraint properties, or within the parameters compartment.
*/
		
		Class ownedBlockClass3=model.createOwnedClass("Block3", false);
		stereotypeApplicationHelper.applyStereotype(ownedBlockClass3, BlocksPackage.eINSTANCE.getBlock(),null);
		
		//The root constraintBlock
		Class constraintBlockClass = model.createOwnedClass("ConstraintBlock", false);
		constraintBlock = (ConstraintBlock) stereotypeApplicationHelper.applyStereotype(constraintBlockClass, ConstraintblocksPackage.eINSTANCE.getConstraintBlock(),null);
		
		Class ownedBlockClass2=model.createOwnedClass("Block2", false);
		stereotypeApplicationHelper.applyStereotype(ownedBlockClass2, BlocksPackage.eINSTANCE.getBlock(),null);
		
		//create a owned property type by a ConstraintBlock
		constraintBlockClass.createOwnedAttribute("cbProperty", constraintBlockClass);
		//create a owned property type by a Block
		constraintBlockClass.createOwnedAttribute("uncbProperty", ownedBlockClass2);
	}

	/**
	 * Tests the '{@link org.eclipse.papyrus.sysml14.constraintblocks.ConstraintBlock#getParameters()
	 * <em>FlowProperty</em>}' feature getter. 
	 * @see org.eclipse.papyrus.sysml14.constraintblocks.ConstraintBlock#getParameters
	 */
	@Test
	public void testGetParameters() {  
		Assert.assertNotNull("Parameters should not be null", constraintBlock.getParameters());
		Assert.assertEquals("Parameters size should be 1", 1, constraintBlock.getParameters().size());
	}

} 
