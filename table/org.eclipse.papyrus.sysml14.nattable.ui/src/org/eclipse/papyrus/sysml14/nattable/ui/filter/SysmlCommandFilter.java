/*****************************************************************************
 * Copyright (c) 2011-2012 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *
 *		CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.nattable.ui.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.papyrus.sysml14.service.types.util.SysMLServiceTypeUtil;
import org.eclipse.papyrus.uml.service.types.element.UMLElementTypes;
import org.eclipse.papyrus.uml.service.types.filter.ICommandFilter;

/**
 * Filter for available Sysml commands. Moved from oep.sysml.modelexplorer
 */
public class SysmlCommandFilter implements ICommandFilter {

	/**
	 * Singleton instance
	 */
	public static final SysmlCommandFilter INSTANCE = new SysmlCommandFilter();

	private List<IElementType> visibleCommands;
	
	private SysmlCommandFilter() {
		// to prevent instantiation
	}

	@Override
	public List<IElementType> getVisibleCommands() {
		if (visibleCommands == null) {
			List<IElementType> localVisibleCommands = new ArrayList<>();
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_BLOCK));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_CONSTRAINTBLOCK));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_FLOWPORT));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_FLOWPROPERTY));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_FLOWSPECIFICATION));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_PROBLEM));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_RATIONALE));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_REQUIREMENT));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_UNIT));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_VALUETYPE));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_VIEW));
			localVisibleCommands.add(ElementTypeRegistry.getInstance().getType(SysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_VIEWPOINT));

			// UMLElementTypes.PROPERTY is required by ConstraintBlock Parameter
			localVisibleCommands.add(UMLElementTypes.PROPERTY);
			this.visibleCommands = Collections.unmodifiableList(localVisibleCommands);
		}
		return visibleCommands;
	}
}
