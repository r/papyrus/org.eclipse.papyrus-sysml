/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Vincent Lorenzo (CEA LIST) vincent.lorenzo@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.nattable.ui.allocation.tester;

import org.eclipse.papyrus.infra.nattable.manager.table.INattableModelManager;
import org.eclipse.papyrus.infra.nattable.utils.NattableWidgetPropertyTester;
import org.eclipse.papyrus.sysml14.nattable.ui.handlers.CreateAllocateHandler;


public class AllocationPropertyTester extends NattableWidgetPropertyTester {

	private static final String IS_ALLOCATION_TABLE = "isAllocationTable"; //$NON-NLS-1$

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		final INattableModelManager manager = getNattableModelManager();
		if (manager != null && IS_ALLOCATION_TABLE.equals(property) && expectedValue instanceof Boolean) {
			return expectedValue.equals(manager.getTable().getTableConfiguration().getType().equals(CreateAllocateHandler.TABLE_ALLOCATION_TYPE));
		}
		return false;
	}
}
