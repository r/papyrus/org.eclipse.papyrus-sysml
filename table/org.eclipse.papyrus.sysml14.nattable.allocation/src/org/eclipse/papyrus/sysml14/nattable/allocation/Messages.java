/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *   
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.nattable.allocation;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME = "org.eclipse.papyrus.sysml14.nattable.allocation.messages"; //$NON-NLS-1$

	public static String TableAllocationCreationTester_NotAnUMLElement;

	public static String TableAllocationCreationTester_OKMessage;

	public static String TableAllocationCreationTester_ProfileNotApplied;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
