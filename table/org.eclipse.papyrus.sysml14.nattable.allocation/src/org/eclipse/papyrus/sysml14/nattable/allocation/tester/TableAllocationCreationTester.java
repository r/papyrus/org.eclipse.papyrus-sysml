/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Vincent Lorenzo (CEA LIST) vincent.lorenzo@cea.fr - Initial API and implementation
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Use the utility method to check SysML 1.4 profile application
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.nattable.allocation.tester;


import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.papyrus.infra.nattable.tester.ITableTester;
import org.eclipse.papyrus.sysml14.nattable.allocation.Activator;
import org.eclipse.papyrus.sysml14.nattable.allocation.Messages;
import org.eclipse.papyrus.sysml14.util.SysMLResource;
import org.eclipse.uml2.uml.Package;



public class TableAllocationCreationTester implements ITableTester {

	/**
	 *
	 * @see org.eclipse.papyrus.infra.nattable.tester.ITableTester#isAllowed(java.lang.Object)
	 *
	 * @param context
	 * @return
	 */
	@Override
	public IStatus isAllowed(Object context) {
		if (context instanceof Package) {
			if (SysMLResource.isSysMLProfileApplied((Package) context)) {
				return new Status(IStatus.OK, Activator.PLUGIN_ID, Messages.TableAllocationCreationTester_OKMessage);
			} else {
				return new Status(IStatus.ERROR, Activator.PLUGIN_ID, String.format(Messages.TableAllocationCreationTester_ProfileNotApplied));
			}
		}
		return new Status(IStatus.ERROR, Activator.PLUGIN_ID, Messages.TableAllocationCreationTester_NotAnUMLElement);
	}

}
