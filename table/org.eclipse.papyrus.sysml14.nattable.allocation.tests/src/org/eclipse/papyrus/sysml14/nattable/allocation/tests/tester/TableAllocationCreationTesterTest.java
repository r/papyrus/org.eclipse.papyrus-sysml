/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.nattable.allocation.tests.tester;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.sysml14.nattable.allocation.tester.TableAllocationCreationTester;
import org.eclipse.papyrus.sysml14.util.SysMLResource;
import org.eclipse.uml2.uml.Model;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test for TableAllocationCreationTester
 */
@SuppressWarnings("nls")
public class TableAllocationCreationTesterTest {

	/**
	 * The creation isn't allowed on null elements
	 */
	@Test
	public void emptyContextTest() {
		TableAllocationCreationTester tableAllocationCreationTester = new TableAllocationCreationTester();
		IStatus allowed = tableAllocationCreationTester.isAllowed(null);
		Assert.assertEquals("The tester should not work on a null entry", IStatus.ERROR, allowed.getSeverity());
	}
	
	/**
	 * The creation of a Table Allocation is allowed on the root of a SyML 1.4 model
	 */
	@Test
	public void simpleModelTest() {
		TableAllocationCreationTester tableAllocationCreationTester = new TableAllocationCreationTester();
		Model model = SysMLResource.createSysMLModel(new ResourceSetImpl());
		Assert.assertFalse("the SysML profil must be applied.", model.getAppliedProfiles().isEmpty());		
		IStatus allowed = tableAllocationCreationTester.isAllowed(model);
		Assert.assertEquals("The tester work on a model with SyML 1.4 profil entry", IStatus.OK, allowed.getSeverity());
	}	



}
