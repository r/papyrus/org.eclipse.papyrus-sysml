/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.nattable.requirement.tests.tester;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.infra.types.core.registries.ElementTypeSetConfigurationRegistry;
import org.eclipse.papyrus.sysml14.nattable.requirement.tester.RequirementTableTester;
import org.eclipse.papyrus.sysml14.requirements.RequirementsPackage;
import org.eclipse.papyrus.sysml14.util.SysMLResource;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.util.UMLUtil.StereotypeApplicationHelper;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Tests on RequirementTableTester
 */
public class RequirementTableTesterTest {

	
	@BeforeClass
	public static void loadElementTypeSet (){
		ElementTypeSetConfigurationRegistry.getInstance();
	}
	
	/**
	 * Check that the tester don't allow null parameter
	 */
	@Test
	public void testNull() {
		RequirementTableTester tableRequirementTester = new RequirementTableTester();
		IStatus emptyNotAllowed = tableRequirementTester.isAllowed(null);
		Assert.assertEquals("The tester should not work on a null entry", IStatus.ERROR, emptyNotAllowed.getSeverity()); //$NON-NLS-1$
	}

	/**
	 * Check that the tester don't allow non profiled parameter
	 */
	@Test
	public void testMissingProfile() {
		RequirementTableTester tableRequirementTester = new RequirementTableTester();
		org.eclipse.uml2.uml.Package pck = UMLFactory.eINSTANCE.createPackage();
		IStatus missingStereotypeNotAllowed = tableRequirementTester.isAllowed(pck);
		Assert.assertEquals("The tester should not work on a non profiled entry", IStatus.ERROR, missingStereotypeNotAllowed.getSeverity()); //$NON-NLS-1$
	}	
	
	
	/**
	 * Check that the tester allow a requirement
	 */
	@Test
	public void testRequirement() {
		RequirementTableTester tableRequirementTester = new RequirementTableTester();
		Model model = SysMLResource.createSysMLModel(new ResourceSetImpl());
		Assert.assertFalse("the SysML profil must be applied.", model.getAppliedProfiles().isEmpty()); //$NON-NLS-1$
		Class requirement = model.createOwnedClass("requirement", false);//$NON-NLS-1$
		StereotypeApplicationHelper stereotypeApplicationHelper = StereotypeApplicationHelper.getInstance(null);
		stereotypeApplicationHelper.applyStereotype(requirement, RequirementsPackage.eINSTANCE.getRequirement(),null);
		IStatus missingStereotypeNotAllowed = tableRequirementTester.isAllowed(requirement);
		Assert.assertEquals("The tester should work on a requirement", IStatus.OK, missingStereotypeNotAllowed.getSeverity()); //$NON-NLS-1$
	}		
	
}
