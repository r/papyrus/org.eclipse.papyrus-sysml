org.eclipse.papyrus.sysml14.architecture
=======================================

* Palettes are defined based on chapters " X.y Diagram Elements"

* SysML uses orange copies of UML icons
** dark  orange: f46800
** light orange: f6b200

* Process to create the new icons: (Should be improved)
** Open the UML . gif with paint.net 
** Change the color
** Save (now the display is correct in model explorer but not in new Diagram/Table menu)
** Open the icon with GIMP
** Export as GIF (now works in both use case)

