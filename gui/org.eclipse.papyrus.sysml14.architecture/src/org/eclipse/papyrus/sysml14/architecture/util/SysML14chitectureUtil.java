/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.architecture.util;

/**
 * Utility class to get informations on DeprecatedElement resources
 *
 */
public final class SysML14chitectureUtil {
	
	private SysML14chitectureUtil(){
	}

    public static final String DIAGRAM_ACTIVITY_ID = "org.eclipse.papyrus.sysml14.diagram.activity"; //$NON-NLS-1$
    public static final String DIAGRAM_BLOCK_DEFINITION_ID = "org.eclipse.papyrus.sysml14.diagram.blockdefinition"; //$NON-NLS-1$	
    public static final String DIAGRAM_INTERNAL_BLOCK_ID = "org.eclipse.papyrus.sysml14.diagram.internalblock"; //$NON-NLS-1$
    public static final String DIAGRAM_PACKAGE_ID = "org.eclipse.papyrus.sysml14.diagram.package"; //$NON-NLS-1$
    public static final String DIAGRAM_PARAMETRIC_ID = "org.eclipse.papyrus.sysml14.diagram.parametric"; //$NON-NLS-1$	
    public static final String DIAGRAM_REQUIREMENT_ID = "org.eclipse.papyrus.sysml14.diagram.requirement"; //$NON-NLS-1$
    public static final String DIAGRAM_SEQUENCE_ID = "org.eclipse.papyrus.sysml14.diagram.sequence"; //$NON-NLS-1$
    public static final String DIAGRAM_STATE_MACHINE_ID = "org.eclipse.papyrus.sysml14.diagram.stateMachine"; //$NON-NLS-1$
    public static final String DIAGRAM_USE_CASE_ID = "org.eclipse.papyrus.sysml14.diagram.useCase"; //$NON-NLS-1$
    
    public static final String TABLE_REQUIREMENT_ID = "org.eclipse.papyrus.sysml14.table.requirement"; //$NON-NLS-1$	
    public static final String TABLE_REQUIREMENT_TREE_ID = "org.eclipse.papyrus.sysml14.table.requirementTree"; //$NON-NLS-1$	
    public static final String TABLE_ALLOCATION_ID = "org.eclipse.papyrus.sysml14.table.allocation"; //$NON-NLS-1$	
	
}
