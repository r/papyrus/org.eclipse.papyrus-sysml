/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.architecture.internal.migration;

import java.util.Collection;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.gmf.runtime.common.core.command.AbstractCommand;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.core.command.IdentityCommand;
import org.eclipse.papyrus.infra.architecture.ArchitectureDomainManager;
import org.eclipse.papyrus.infra.core.architecture.ArchitectureDescription;
import org.eclipse.papyrus.infra.core.architecture.RepresentationKind;
import org.eclipse.papyrus.infra.core.architecture.merged.MergedArchitectureContext;
import org.eclipse.papyrus.infra.core.architecture.merged.MergedArchitectureDescriptionLanguage;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.resource.sasheditor.DiModelUtils;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.infra.nattable.common.reconciler.TableReconciler;
import org.eclipse.papyrus.infra.nattable.model.nattable.Table;
import org.eclipse.papyrus.infra.nattable.representation.PapyrusTable;
import org.eclipse.papyrus.infra.viewpoints.configuration.PapyrusView;
import org.eclipse.papyrus.sysml14.architecture.util.SysML14chitectureUtil;

/**
 * SysML 1.4 Table Reconciler from Neon to Oxygen that replaces the old
 * prototype value for tables with ones based on table kinds (Will move Table
 * version annotation from 1.0.0 to 1.3.0)
 */
@SuppressWarnings({ "deprecation", "nls" }) // Warning are not pertinent this migration should remains on old API
public class SysMLNeonToOxygenTableReconciler extends TableReconciler {

	private static final String ALLOCATION_TABLE_URI = "platform:/plugin/org.eclipse.papyrus.sysml14.nattable.allocation/resources/configuration/allocationTable.configuration#_j2LQIC7uEeWklMXvaoXzgQ";
	private static final String REQUIREMENT_TABLE_URI = "platform:/plugin/org.eclipse.papyrus.sysml14.nattable.requirement/resources/configuration/requirementTable.configuration#_j2LQIy7uEeWklMXvaoXzgQ";
	private static final String REQUIREMENT_TREE_TABLE_URI = "platform:/plugin/org.eclipse.papyrus.sysml14.nattable.requirement/resources/configuration/requirementTable.configuration#_eXTo8CywEeaHK-6t3D9x9w";

	private static final String SYSML14_CONTEXT_ID = "org.eclipse.papyrus.sysml.architecture.SysML14";	
	
	@Override
	public ICommand getReconcileCommand(Table table) {
		if (table.getPrototype() instanceof PapyrusView) {
			PapyrusView oldTableKind = (PapyrusView) table.getPrototype();
			if (oldTableKind != null) {
				URI uri = EcoreUtil.getURI(oldTableKind);
				if (uri != null) {
					RepresentationKind newTableKind = null;
					switch (uri.toString()) {
					case ALLOCATION_TABLE_URI:
						newTableKind = getPapyrusTableById(SysML14chitectureUtil.TABLE_ALLOCATION_ID);
						break;
					case REQUIREMENT_TABLE_URI:
						newTableKind = getPapyrusTableById(SysML14chitectureUtil.TABLE_REQUIREMENT_ID);
						break;
					case REQUIREMENT_TREE_TABLE_URI:
						newTableKind = getPapyrusTableById(SysML14chitectureUtil.TABLE_REQUIREMENT_TREE_ID);
						break;
					default:
						break;
					}
					if (newTableKind != null) {
						ReplaceTablePrototypeCommand replaceTablePrototypeCommand = new ReplaceTablePrototypeCommand(table, newTableKind);
						Resource eResource = table.eResource();
						if (eResource != null) {
							ResourceSet resourceSet = eResource.getResourceSet();
							if (resourceSet instanceof ModelSet) {
								CompositeCommand compositeCommand = new CompositeCommand("Update viewpoints from configuration to architecture and set SysML 1.4 as default architecture");
								compositeCommand.add(replaceTablePrototypeCommand);
								Command setContextCommand = getSetContextCommand( (ModelSet) resourceSet, SYSML14_CONTEXT_ID);
								compositeCommand.add(EMFtoGMFCommandWrapper.wrap(setContextCommand));
								return compositeCommand;
							}				
						}						
						return replaceTablePrototypeCommand;
					}					
				}
			}
		}
		return IdentityCommand.INSTANCE;
	}

	////////////////////////////////////////////////////////////////////////////////
	// FIXME: should be in Papyrus core API
	
	protected Command getSetContextCommand(ModelSet modelSet, String contextId) {
		return new RecordingCommand(modelSet.getTransactionalEditingDomain()) {
			@Override
			protected void doExecute() {
				ArchitectureDescription description = DiModelUtils.getOrAddArchitectureDescription(modelSet);
				description.setContextId(contextId);
			}
		};
	}
	
	protected PapyrusTable getPapyrusTableById(String id) {
		ArchitectureDomainManager manager = ArchitectureDomainManager.getInstance();
		Collection<MergedArchitectureContext> visibleArchitectureContexts = manager.getVisibleArchitectureContexts();
		for (MergedArchitectureContext mergedArchitectureContext : visibleArchitectureContexts) {
			if (mergedArchitectureContext instanceof MergedArchitectureDescriptionLanguage) {
				MergedArchitectureDescriptionLanguage mergedArchitectureDescriptionLanguage = (MergedArchitectureDescriptionLanguage) mergedArchitectureContext;
				for (RepresentationKind representationKind : mergedArchitectureDescriptionLanguage
						.getRepresentationKinds()) {
					if (representationKind instanceof PapyrusTable && representationKind.getId().equals(id)) {
						return (PapyrusTable) representationKind;
					}
				}
			}
		}
		return null;
	}

	/**
	 * A command to replace the old table prototype with the new representation
	 * kinds
	 */
	protected class ReplaceTablePrototypeCommand extends AbstractCommand {

		private Table table;
		private RepresentationKind newKind;

		public ReplaceTablePrototypeCommand(Table table, RepresentationKind newKind) {
			super("Replace the SysML 1.4 Table configuration from neon to oxygen");
			this.table = table;
			this.newKind = newKind;
		}

		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info)
				throws ExecutionException {
			table.setPrototype(null);
			table.setTableKindId(newKind.getId());
			return CommandResult.newOKCommandResult();
		}

		@Override
		public boolean canUndo() {
			return false;
		}

		@Override
		public boolean canRedo() {
			return false;
		}

		@Override
		protected CommandResult doRedoWithResult(IProgressMonitor progressMonitor, IAdaptable info)
				throws ExecutionException {
			throw new ExecutionException("Should not be called, canRedo false"); //$NON-NLS-1$
		}

		@Override
		protected CommandResult doUndoWithResult(IProgressMonitor progressMonitor, IAdaptable info)
				throws ExecutionException {
			throw new ExecutionException("Should not be called, canUndo false"); //$NON-NLS-1$
		}
	}
	////////////////////////////////////////////////////////////////////////////////
}
