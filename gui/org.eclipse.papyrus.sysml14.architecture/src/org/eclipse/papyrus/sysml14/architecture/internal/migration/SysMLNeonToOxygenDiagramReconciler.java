/*****************************************************************************
 * Copyright (c) 2017 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.architecture.internal.migration;

import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.papyrus.infra.architecture.ArchitectureDomainManager;
import org.eclipse.papyrus.infra.core.architecture.ArchitectureDescription;
import org.eclipse.papyrus.infra.core.architecture.RepresentationKind;
import org.eclipse.papyrus.infra.core.architecture.merged.MergedArchitectureContext;
import org.eclipse.papyrus.infra.core.architecture.merged.MergedArchitectureDescriptionLanguage;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.resource.sasheditor.DiModelUtils;
import org.eclipse.papyrus.infra.emf.gmf.command.EMFtoGMFCommandWrapper;
import org.eclipse.papyrus.infra.gmfdiag.common.reconciler.DiagramReconciler_1_3_0;
import org.eclipse.papyrus.infra.gmfdiag.representation.PapyrusDiagram;
import org.eclipse.papyrus.infra.viewpoints.configuration.PapyrusView;
import org.eclipse.papyrus.infra.viewpoints.style.PapyrusViewStyle;
import org.eclipse.papyrus.sysml14.architecture.util.SysML14chitectureUtil;

/**
 * SysML 1.4 Diagram Reconciler from Neon to Oxygen that switches the old
 * PapyrusViewStyle by the new PapyrusDiagramStyle (Will move Diagram version
 * annotation from 1.2.0 to 1.3.0)
 */
@SuppressWarnings({ "deprecation", "nls" }) // Warning are not pertinent this migration should remains on old deprecated API
public class SysMLNeonToOxygenDiagramReconciler extends DiagramReconciler_1_3_0 {
	
	private static final String BLOCK_DEFINITION_DIAGRAM_URI = "platform:/plugin/org.eclipse.papyrus.sysml14.diagram.blockdefinition/resources/configuration/blockDefinitionDiagram.configuration#_ZrBf4JaBEeS8eNvSwD0lgA";
	private static final String INTERNAL_BLOCK_DIAGRAM_URI = "platform:/plugin/org.eclipse.papyrus.sysml14.diagram.internalblock/resources/configuration/internalBlockDiagram.configuration#_ZrBf4JaBEeS8eNvSwD0lgA";
	private static final String PARAMETRIC_DIAGRAM_URI = "platform:/plugin/org.eclipse.papyrus.sysml14.diagram.parametric/resources/configuration/parametricDiagram.configuration#_ZrBf4JaBEeS8eNvSwD0lgA";
	private static final String REQUIREMENT_DIAGRAM_URI = "platform:/plugin/org.eclipse.papyrus.sysml14.diagram.requirement/resources/configuration/requirementDiagram.configuration#_ZrBf4JaBEeS8eNvSwD0lgA";

	private static final String SYSML14_CONTEXT_ID = "org.eclipse.papyrus.sysml.architecture.SysML14";
	
	/**
	 * @see org.eclipse.papyrus.infra.gmfdiag.common.reconciler.DiagramReconciler_1_3_0#getPapyrusDiagram(org.eclipse.papyrus.infra.viewpoints.style.PapyrusViewStyle)
	 *
	 * @param oldStyle
	 * @return
	 */
	@Override
	protected PapyrusDiagram getDiagramKind(Diagram diagram, PapyrusViewStyle oldStyle) {
		if (oldStyle != null) {
			PapyrusView configuration = oldStyle.getConfiguration();
			if (configuration != null) {
				URI uri = EcoreUtil.getURI(configuration);
				if (uri != null) {
					switch (uri.toString()) {
					case REQUIREMENT_DIAGRAM_URI:
						return getPapyrusDiagramById(SysML14chitectureUtil.DIAGRAM_REQUIREMENT_ID);
					case BLOCK_DEFINITION_DIAGRAM_URI:
						return getPapyrusDiagramById(SysML14chitectureUtil.DIAGRAM_BLOCK_DEFINITION_ID);
					case INTERNAL_BLOCK_DIAGRAM_URI:
						return getPapyrusDiagramById(SysML14chitectureUtil.DIAGRAM_INTERNAL_BLOCK_ID);
					case PARAMETRIC_DIAGRAM_URI:
						return getPapyrusDiagramById(SysML14chitectureUtil.DIAGRAM_PARAMETRIC_ID);
					default:// not a SysML 1.4 Diagram configuration
						break;
					}					
				}
			}

		}
		return null;
	}

	@Override
	public ICommand getReconcileCommand(Diagram diagram) {
		ICommand reconcileCommand = super.getReconcileCommand(diagram);
		if (reconcileCommand != null && diagram != null) {
			Resource eResource = diagram.eResource();
			if (eResource != null) {
				ResourceSet resourceSet = eResource.getResourceSet();
				if (resourceSet instanceof ModelSet) {
					CompositeCommand compositeCommand = new CompositeCommand("Update viewpoints from configuration to architecture and set SysML 1.4 as default architecture");
					compositeCommand.add(reconcileCommand);
					Command setContextCommand = getSetContextCommand((ModelSet) resourceSet, SYSML14_CONTEXT_ID);
					compositeCommand.add(EMFtoGMFCommandWrapper.wrap(setContextCommand));
					return compositeCommand;
				}
			}
		}
		return reconcileCommand;
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// FIXME: should be in Papyrus core API
	protected Command getSetContextCommand(ModelSet modelSet, String contextId) {
		return new RecordingCommand(modelSet.getTransactionalEditingDomain()) {
			@Override
			protected void doExecute() {
				ArchitectureDescription description = DiModelUtils.getOrAddArchitectureDescription(modelSet);
				description.setContextId(contextId);
			}
		};
	}	
	
	protected PapyrusDiagram getPapyrusDiagramById(String id) {
		ArchitectureDomainManager manager = ArchitectureDomainManager.getInstance();
		Collection<MergedArchitectureContext> visibleArchitectureContexts = manager.getVisibleArchitectureContexts();
		for (MergedArchitectureContext mergedArchitectureContext : visibleArchitectureContexts) {
			if (mergedArchitectureContext instanceof MergedArchitectureDescriptionLanguage) {
				MergedArchitectureDescriptionLanguage mergedArchitectureDescriptionLanguage = (MergedArchitectureDescriptionLanguage) mergedArchitectureContext;
				for (RepresentationKind representationKind : mergedArchitectureDescriptionLanguage
						.getRepresentationKinds()) {
					if (representationKind instanceof PapyrusDiagram && representationKind.getId().equals(id)) {
						return (PapyrusDiagram) representationKind;
					}
				}
			}
		}
		return null;
	}
	////////////////////////////////////////////////////////////////////////////////
}
