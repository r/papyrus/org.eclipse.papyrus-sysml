/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * Benoit Maggi benoit.maggi@cea.fr - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.ui.template;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.uml.diagram.wizards.transformation.IGenerator;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.papyrus.sysml14.ui.Activator;
import org.eclipse.uml2.uml.PackageImport;


/**
 * Based on org.eclipse.papyrus.uml.templaterepository.ImportUMLPrimitiveTypesInModel
 * @deprecated
 */
@Deprecated //FIXME unused since the standard profile is in the default created model 0.8.1 (What about adding it in papyrus core?)
public class ImportUMLStandardProfileInModel implements IGenerator {


	private ModelSet modelSet;

	private org.eclipse.uml2.uml.Package rootElement;

	private EObject standardProfile;

	/**
	 * @see org.eclipse.papyrus.uml.diagram.wizards.transformation.IGenerator#setModelSet(org.eclipse.papyrus.uml.diagram.wizards.transformation.ModelSet)
	 *
	 * @param modelSet
	 */
	@Override
	public void setModelSet(ModelSet modelSet) {
		this.modelSet = modelSet;
	}

	/**
	 * @param modelSet
	 *            the current model set
	 * @return the associated command stack
	 */
	protected final CommandStack getCommandStack(ModelSet modelSet) {
		return modelSet.getTransactionalEditingDomain().getCommandStack();
	}

	/**
	 * @return the model set
	 */
	public ModelSet getModelSet() {
		return this.modelSet;
	}

	/**
	 * @see org.eclipse.papyrus.uml.diagram.wizards.transformation.IGenerator#execute()
	 *
	 */
	@Override
	public void execute() {
		Resource umlResource = UmlUtils.getUmlModel(modelSet).getResource();

		// Select the root element to add the wanted imports
		rootElement = (org.eclipse.uml2.uml.Model) umlResource.getContents().get(0);

		// Load the needed libraries. 
		URI standardProfileURI = URI.createURI("pathmap://UML_PROFILES/Standard.profile.uml").appendFragment("_0");  //$NON-NLS-1$ //$NON-NLS-2$
		standardProfile = modelSet.getEObject(standardProfileURI, true);

		// Creates the import packages at the root of the model (elements of type packageImport)
		this.getCommandStack(modelSet).execute(new RecordingCommand(modelSet.getTransactionalEditingDomain()) {

			@Override
			protected void doExecute() {

				PackageImport importPrimitivePack = rootElement.createPackageImport((org.eclipse.uml2.uml.Package) standardProfile);
				if (!rootElement.getPackageImports().contains(importPrimitivePack)) {
					rootElement.getPackageImports().add(importPrimitivePack);
				}

				try {
					rootElement.eResource().save(Collections.emptyMap());
				} catch (IOException e) {
					Activator.getLogHelper().error(e);
				}
			}
		});

	}



}
