/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Francois Le Fevre francois.le-fevre@cea.fr  - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.ui.queries.property;

import org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager;
import org.eclipse.papyrus.emf.facet.efacet.core.exception.DerivedTypedElementException;
import org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2;
import org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2;
import org.eclipse.uml2.uml.Property;

/**
 * Query used for Menu Explorer customisation
 * @pap.req org.eclipse.papyrus.sysml14.ui#REQ_001
 * 
 * @author Francois Le Fevre
 *
 */
public class IsTypedPropertyJavaQuery implements IJavaQuery2<Property, Boolean> {

	/* (non-Javadoc)
	 * @see org.eclipse.papyrus.emf.facet.query.java.core.IJavaQuery2#evaluate(org.eclipse.emf.ecore.EObject, org.eclipse.papyrus.emf.facet.query.java.core.IParameterValueList2, org.eclipse.papyrus.emf.facet.efacet.core.IFacetManager)
	 * 
	 * Return true if the element is a typed property
	 */
	@Override
	public Boolean evaluate(Property source, IParameterValueList2 parameterValues, IFacetManager facetManager) throws DerivedTypedElementException {
		return source.getType()!=null;
	}

}
