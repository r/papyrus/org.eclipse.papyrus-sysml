/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *  
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.diagram.common.tests.util;

import java.util.Map;

import org.eclipse.papyrus.infra.types.ElementTypeSetConfiguration;
import org.eclipse.papyrus.infra.types.core.registries.ElementTypeSetConfigurationRegistry;
import org.eclipse.papyrus.sysml14.diagram.common.util.GraphicalSysMLServiceTypeUtil;
import org.eclipse.papyrus.sysml14.service.types.util.SysMLServiceTypeUtil;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Check constant of GraphicalSysMLServiceTypeUtil
 */
@SuppressWarnings("nls")
public class GraphicalSysMLServiceTypeUtilTest {

	@BeforeClass
	public static void loadSysMLDefinition() {
		ElementTypeSetConfigurationRegistry instance = ElementTypeSetConfigurationRegistry.getInstance();
		Map<String, Map<String, ElementTypeSetConfiguration>> elementTypeSetConfigurations = instance.getElementTypeSetConfigurations();
		Map<String, ElementTypeSetConfiguration> map = elementTypeSetConfigurations.get(SysMLServiceTypeUtil.SYSML14_CONTEXT_ID);
		ElementTypeSetConfiguration elementTypeSetConfiguration = map.get("org.eclipse.papyrus.sysml14di.elementTypes");
		Assert.assertNotNull("The SysML element type set definition is missing", elementTypeSetConfiguration);
	}
	

	@Test
	public void checkConstraintLabelElementTypeId() {
		Assert.assertTrue("ORG_ECLIPSE_PAPYRUS_SYSML14_CONSTRAINT_LABEL should have a valid element id : " , SysMLServiceTypeUtil.isKnown(GraphicalSysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_CONSTRAINT_LABEL));
	}	
	
	@Test
	public void checkParameterBorderItemLabelElementTypeId() {
		Assert.assertTrue("ORG_ECLIPSE_PAPYRUS_SYSML14_PARAMETER_BORDER_ITEM should have a valid element id : " , SysMLServiceTypeUtil.isKnown(GraphicalSysMLServiceTypeUtil.ORG_ECLIPSE_PAPYRUS_SYSML14_PARAMETER_BORDER_ITEM));
	}	

}
