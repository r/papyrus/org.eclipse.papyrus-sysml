/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 *    
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi (CEA LIST) benoit.maggi@cea.fr - Initial API and implementation
 *  
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.diagram.common.tests.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.papyrus.infra.types.ElementTypeConfiguration;
import org.eclipse.papyrus.infra.types.ElementTypeSetConfiguration;
import org.eclipse.papyrus.infra.types.core.registries.ElementTypeSetConfigurationRegistry;
import org.eclipse.papyrus.sysml14.service.types.util.SysMLServiceTypeUtil;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * This class is to test the validity of sysmldi element types
 */
@SuppressWarnings("nls")
public class SysMLDiUMLTest {

	public static final String SYSMLDI_ELEMENT_TYPE_PATH = "org.eclipse.papyrus.sysml14.diagram.common/resources/SysMLDiUML.elementtypesconfigurations";

	@BeforeClass
	public static void loadSysMLDefinition() {
		ElementTypeSetConfigurationRegistry instance = ElementTypeSetConfigurationRegistry.getInstance();
		Map<String, Map<String, ElementTypeSetConfiguration>> elementTypeSetConfigurations = instance.getElementTypeSetConfigurations();
		Map<String, ElementTypeSetConfiguration> map = elementTypeSetConfigurations.get(SysMLServiceTypeUtil.SYSML14_CONTEXT_ID);
		ElementTypeSetConfiguration elementTypeSetConfiguration = map.get("org.eclipse.papyrus.sysml14di.elementTypes");
		Assert.assertNotNull("The SysML element type set definition is missing", elementTypeSetConfiguration);
	}

	/**
	 * Validate the model with the rules defined in the meta-model tooling
	 */

	@Test
	public void validateSysML14ElementTypesModel() {
		URI createPlatformPluginURI = URI.createPlatformPluginURI(SYSMLDI_ELEMENT_TYPE_PATH, true);
		Resource resource = new ResourceSetImpl().getResource(createPlatformPluginURI, true);
		Diagnostic diagnostic = Diagnostician.INSTANCE.validate(resource.getContents().get(0));
		Assert.assertEquals("The element type model is not valid ", Diagnostic.OK, diagnostic.getSeverity());
	}

	/**
	 * Check if there is a duplicated identifier in all loaded element types
	 */
	@Test
	public void checkAlreadyExistingId() {
		ElementTypeSetConfigurationRegistry instance = ElementTypeSetConfigurationRegistry.getInstance();
		Map<String, Map<String, ElementTypeSetConfiguration>> elementTypeSetConfigurations = instance.getElementTypeSetConfigurations();
		Map<String, ElementTypeSetConfiguration> map = elementTypeSetConfigurations.get(SysMLServiceTypeUtil.SYSML14_CONTEXT_ID);
		List<String> alreadyCheckedIds = new ArrayList<>();
		for (ElementTypeSetConfiguration elementTypeSetConfiguration : map.values()) {
			EList<ElementTypeConfiguration> elementTypeConfigurations = elementTypeSetConfiguration.getElementTypeConfigurations();
			for (ElementTypeConfiguration elementTypeConfiguration : elementTypeConfigurations) {
				String identifier = elementTypeConfiguration.getIdentifier();
				Assert.assertFalse("Duplicated elementtype identifer " + identifier, alreadyCheckedIds.contains(identifier));
				alreadyCheckedIds.add(identifier);
			}
		}
	}

}
