/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.diagram.common.figure;

import org.eclipse.swt.graphics.Image;

/**
 * This figure is a specialization of a connector in order to overload the display of the BindingConnector display
 * This class satisfies the requirement{@papyrus.req org.eclipse.papyrus.sysml14.diagram.internalblock#Req_004}
 *
 */
public class SysMLConnectorFigure extends org.eclipse.papyrus.uml.diagram.composite.custom.figures.ConnectorFigure {

	/**
	 * The String that must display a bindingConnector
	 */
	protected static final String STEREOTYPE_LABEL_BINDINGCONNECTOR = "equal";
	/**
	 * String that replace the name of the stereotype BindingConnector
	 */
	protected static final String BINDING_CONNECTOR = "BindingConnector";

	/**
	 * @see org.eclipse.papyrus.uml.diagram.common.figure.edge.UMLEdgeFigure#setStereotypeDisplay(java.lang.String, org.eclipse.swt.graphics.Image)
	 *
	 * @param stereotypes
	 * @param image
	 */
	@Override
	public void setStereotypeDisplay(String stereotypes, Image image) {
		String newStereotypeString = stereotypes.replaceAll(BINDING_CONNECTOR, STEREOTYPE_LABEL_BINDINGCONNECTOR);
		super.setStereotypeDisplay(newStereotypeString, image);
	}

}
