/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Patrick Tessier (CEA LIST) Patrick.tessier@cea.fr - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.diagram.common.util;

/**
 * List SysML 1.4 graphical element type ids
 *
 */
public final class GraphicalSysMLServiceTypeUtil {

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_PARAMETER_BORDER_ITEM = "org.eclipse.papyrus.sysmldi.Parameter_BorderItem"; //$NON-NLS-1$

	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CONSTRAINT_LABEL = "org.eclipse.papyrus.sysmldi.Constraint_Label";//$NON-NLS-1$
	
	public static final String ORG_ECLIPSE_PAPYRUS_SYSML14_CONSTRAINT_PROPERTY_COMPOSITE = "org.eclipse.papyrus.sysmldi.ConstraintPropertyComposite"; //$NON-NLS-1$
	
	private GraphicalSysMLServiceTypeUtil() {
	}

}
