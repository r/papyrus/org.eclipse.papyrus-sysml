/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.diagram.common.edit.parts;

import org.eclipse.draw2d.Connection;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.sysml14.diagram.common.figure.SysMLConnectorFigure;

/**
 * This editpart has been specialized in order to replace default figure by a custom figure to change display of stereotype application.
 * This class satisfies the requirement{@papyrus.req org.eclipse.papyrus.sysml14.diagram.internalblock#Req_004}
 */
public class SysMLConnectorEditPart extends org.eclipse.papyrus.uml.diagram.composite.edit.parts.ConnectorEditPart {

	/**
	 * Constructor.
	 *
	 * @param view
	 */
	public SysMLConnectorEditPart(View view) {
		super(view);
	}

	@Override
	protected Connection createConnectionFigure() {
		return new SysMLConnectorFigure();
	}

}
