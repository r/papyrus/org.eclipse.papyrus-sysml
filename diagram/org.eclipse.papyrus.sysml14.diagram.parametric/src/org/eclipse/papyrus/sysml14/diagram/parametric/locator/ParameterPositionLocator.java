/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.diagram.parametric.locator;

import org.eclipse.draw2d.IFigure;
import org.eclipse.papyrus.infra.gmfdiag.common.utils.PortPositionEnum;
import org.eclipse.papyrus.uml.diagram.common.locator.PortPositionLocator;


/**
 * This border locator do not take in account CSS, its always internal.
 * So For this reason SertPortPosition() has been overloaded.
 *
 */
public class ParameterPositionLocator extends PortPositionLocator {


	/**
	 * Constructor.
	 * @param parentFigure
	 */
	public ParameterPositionLocator(IFigure parentFigure) {
		super(parentFigure);
	}		
	
	/**
	 * @see org.eclipse.papyrus.uml.diagram.common.locator.PortPositionLocator#setPortPosition(java.lang.String)
	 *
	 * @param position
	 * @deprecated (to be removed only when the deprecated parent is removed)
	 */
	@Override
	@Deprecated
	public void setPortPosition(String position) {
		// Always internal
		super.setPortPosition(PortPositionEnum.INSIDE.toString());
	}
	
	/**
	 * @see org.eclipse.papyrus.uml.diagram.common.locator.PortPositionLocator#setPosition(org.eclipse.papyrus.infra.gmfdiag.common.utils.PortPositionEnum)
	 *
	 * @param position
	 */
	@Override
	public void setPosition(PortPositionEnum position) {
		super.setPosition(PortPositionEnum.INSIDE);
	}
	
}
