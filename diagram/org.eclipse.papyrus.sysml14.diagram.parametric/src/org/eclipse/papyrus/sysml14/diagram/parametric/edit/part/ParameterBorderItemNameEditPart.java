/*****************************************************************************
 * Copyright (c) 2015 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.sysml14.diagram.parametric.edit.part;

import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.infra.gmfdiag.common.editpart.IControlParserForDirectEdit;
import org.eclipse.papyrus.uml.diagram.common.editparts.BorderItemNameEditPart;

/**
 * This class is fix in order to let the xtext parser to change the parser of the editpart.
 *
 */
public class ParameterBorderItemNameEditPart extends BorderItemNameEditPart implements IControlParserForDirectEdit {

	/**
	 * Constructor.
	 * 
	 * @param view
	 */
	public ParameterBorderItemNameEditPart(View view) {
		super(view);
	}
}
