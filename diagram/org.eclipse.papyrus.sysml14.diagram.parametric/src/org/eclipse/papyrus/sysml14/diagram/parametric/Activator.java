/*****************************************************************************
 * Copyright (c) 2015 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Benoit Maggi benoit.maggi@cea.fr  - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.sysml14.diagram.parametric;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.eclipse.papyrus.sysml14.diagram.parametric"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	/**
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 *
	 * @param context
	 * @throws Exception
	 */
	@Override //FIXME : remove the code after Papyrus Core Oxygen.2 (See Bug 522295)
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		IEclipsePreferences node = InstanceScope.INSTANCE.getNode("org.eclipse.papyrus.infra.gmfdiag.dnd"); //$NON-NLS-1$
		node.put("org.eclipse.papyrus.infra.gmfdiag.dnd.expansiondropsteategy.isActive", "false"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}	
	
	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Log the status
	 * @param status
	 */
	public static void log(IStatus status) {
		Activator.getDefault().getLog().log(status);
	}

	/**
	 * Log the message
	 * @param severity
	 * @param message
	 */
	public static void log(int severity, String message) {
		log(new Status(severity, PLUGIN_ID, message));
	}	
		
	
}
