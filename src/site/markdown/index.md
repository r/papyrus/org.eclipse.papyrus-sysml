#### [Papyrus][epapyrus]

[![Modeling environment with Papyrus](images/carousel/carousel-papyrus.png)][epapyrus]

Our application is a component of [Papyrus][epapyrus], an integrated and user-consumable environment for editing any kind of EMF model and particularly supporting UML2 and related modeling languages .

#### [QUDV][equdv]

[![Quantities, Units, Dimensions, Values QUDV](images/carousel/carousel-qudv.png)][equdv]

Use [QUDV][equdv] to define systems of units and quantities for use in system models.


#### [Element-types][eelementtypes]

[![Element-types](images/carousel/carousel-elementtypes.png)][eelementtypes]

Papyrus SysML 1.4 uses the Element types framework to manage the whole life cycle of objects.


#### [SysML profile][eprofile]

[![Profile](images/carousel/carousel-profile.png)][eprofile]

Papyrus SysML 1.4 is based upon the OMG SysML 1.4 standard.


#### [SysML Requirements][ereq]

[![Requirements diagram](images/carousel/carousel-requirements.png)][ereq]

You can define your requirements directly with table format.


[equdv]: http://www.omgwiki.org/OMGSysML/doku.php?id=sysml-qudv:quantities_units_dimensions_values_qudv
[eelementtypes]: http://wiki.eclipse.org/Papyrus_User_Guide
[eprofile]: http://www.omg.org/spec/SysML/1.4/
[ereq]: http://www.omg.org/spec/SysML/1.4/


---


## SysML 1.4, an extension of [Papyrus][epapyrus]


This site is mainly dedicated for developers, you can find information relative to the SysML's developments, such as [Git][egit], Dependencies, [Javadoc][ejavadoc], procedures, tips etc...
You can use the download menu to get the official RCP or use the updatesite url.

## Documentation

Have a look at the developer documentation: [site][devhome] or [pdf][developer-guide.pdf].

Have a look at the user documentation: [site][userhome] or [pdf][user-guide.pdf].

[devhome]: ./developer/index.html
[developer-guide.pdf]: ./pdf/developer-guide.pdf
[userhome]: ./user/index.html
[user-guide.pdf]: ./pdf/user-guide.pdf
[ejavadoc]: ./xref/
[egit]: ./source-repository.html


---


### About

SysML14 is a component of the Eclipse [Papyrus][epapyrus]'s galaxy. SysML 1.4 application is a Papyrus DSML implementing the [SysML 1.4 OMG standard][eomg].

[epapyrus]: https://eclipse.org/papyrus/
[eomg]: http://www.omg.org/spec/SysML/1.4/
